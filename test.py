from pymongo import MongoClient


def get_users():
    client = MongoClient()
    db = client['BotDB']
    _users = db['Users']
    return _users


def add_new_user(_new_id):
    users = get_users()
    users.update_one(
        {"id": _new_id},
        {
            "$setOnInsert": {"id": _new_id, "state": "normal"}
        },
        upsert=True
    )


def main():
    a = 5


main()
